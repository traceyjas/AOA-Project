package com.example.jasmine.aoa;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private CustomArrayList personList = new CustomArrayList();
    private RecyclerView recyclerView;
    private AddressAdapter mAdapter;

    public void findPerson(View view){

        Collections.sort(personList);

        EditText firstnameText = (EditText) findViewById(R.id.firstnameTextField);
        EditText lastnameText = (EditText) findViewById(R.id.lastnameTextField);

        Timer timer = new Timer();
        ArrayList<Person> list;

        timer.start();
        list = personList.search(firstnameText.getText().toString(), lastnameText.getText().toString());
        timer.end();

        long binaryTime = timer.getTotalTimeSeconds();

        timer.start();
        personList.LinearSearch(firstnameText.getText().toString(), lastnameText.getText().toString());
        timer.end();

        long linearTime = timer.getTotalTimeSeconds();

        timer.start();
        BoyerMooreSearch(firstnameText.getText().toString(), lastnameText.getText().toString());
        timer.end();

        long boyerTime = timer.getTotalTimeSeconds();

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog);

        TextView binary = (TextView) dialog.findViewById(R.id.binaryTextView);
        binary.setText(binaryTime + "ms");

        TextView linear = (TextView) dialog.findViewById(R.id.linearTextView);
        linear.setText(linearTime + "ms");

        TextView boyer = (TextView) dialog.findViewById(R.id.boyerTextView);
        boyer.setText(boyerTime + "ms");

        dialog.show();

        if (list.isEmpty())
            Toast.makeText(MainActivity.this, "Person does not exist", Toast.LENGTH_LONG).show();
        else
            recyclerView.setAdapter(new AddressAdapter(list));
            recyclerView.invalidate();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new AddressAdapter(personList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        preparePersonData();
    }

    private void preparePersonData() {
        BufferedReader br = null;
        try
        {
            InputStream inputStream = getResources().openRawResource(R.raw.samplefile);
            br = new BufferedReader(new InputStreamReader(inputStream));

            String line = "";
            //Read to skip the header
            br.readLine();
            //Reading from the second line
            while ((line = br.readLine()) != null)
            {
                String[] employeeDetails = line.split(",");

                if(employeeDetails.length > 0 )
                {
                    //Save the person details in Person Arraylist
                    Person person = new Person(employeeDetails[0], employeeDetails[1],employeeDetails[2]);
                    personList.add(person);
                }
            }
        }
        catch(Exception ee)
        {
            ee.printStackTrace();
        }
        finally
        {
            try
            {
                br.close();
            }
            catch(IOException ie)
            {
                System.out.println("Error occured while closing the BufferedReader");
                ie.printStackTrace();
            }
        }

        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        mAdapter.notifyDataSetChanged();
    }

    CustomArrayList<Person> BoyerMooreSearch(String firstname, String lastname) {
        CustomArrayList<Person> persons = new CustomArrayList<>();

        String text = firstname+lastname;

        BoyerMoore boyerMoore = new BoyerMoore();

        BufferedReader br = null;

        try
        {
            InputStream inputStream = getResources().openRawResource(R.raw.samplefile);
            br = new BufferedReader(new InputStreamReader(inputStream));

            String line = "";
            while ((line = br.readLine()) != null)
            {
                String[] employeeDetails = line.split(",");

                if(employeeDetails.length > 0)
                {
                    String name = employeeDetails[0]+employeeDetails[1];

                    if(boyerMoore.findPattern(text, name) == 1)
                        persons.add(new Person(employeeDetails[0], employeeDetails[1],employeeDetails[2]));
                }
            }
        }
        catch(Exception ee)
        {
            ee.printStackTrace();
        }
        finally
        {
            try
            {
                br.close();
            }
            catch(IOException ie)
            {
                System.out.println("Error occured while closing the BufferedReader");
                ie.printStackTrace();
            }
        }

        return persons;
    }
}
