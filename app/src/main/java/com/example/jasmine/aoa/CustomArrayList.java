package com.example.jasmine.aoa;

import java.util.ArrayList;
import java.util.Collections;

public class CustomArrayList<T> extends ArrayList<T> {

    public ArrayList<Person> search(String firstname, String lastname){

        ArrayList<Person> persons = new ArrayList<>();

        int lowerBound = lower_bound(firstname, lastname, 0, this.size() - 1);
        int upperBound = upper_bound(firstname, lastname, lowerBound, this.size() - 1);

        for (int i = lowerBound; i < upperBound; i++)
            persons.add((Person) this.get(i));

        return persons;
    }

    private int lower_bound(String firstname, String lastname, int low, int high)
    {
        Person search = new Person(firstname, lastname, null);

        if (low > high)
            //return -1;
            return low;

        int mid = low + ((high - low) >> 1);

        //Attention here, we go left for lower_bound when meeting equal values
        if (((Person)this.get(mid)).compareTo(search) >= 0)
            return lower_bound(firstname, lastname, low, mid - 1);
        else
            return lower_bound(firstname, lastname, mid + 1, high);
    }

    private int upper_bound(String firstname, String lastname, int low, int high)
    {
        Person search = new Person(firstname, lastname, null);

        if (low > high)
            //return -1;
            return low;

        int mid = low + ((high - low) >> 1);

        //Attention here, we go right for upper_bound when meeting equal values
        if (((Person)this.get(mid)).compareTo(search) > 0)
            return upper_bound(firstname, lastname, low, mid - 1);
        else
            return upper_bound(firstname, lastname, mid + 1, high);
    }

    ArrayList<Person> LinearSearch(String firstname, String lastname) {
        ArrayList<Person> persons = new ArrayList<>();
        Person search = new Person(firstname, lastname, null);

        for(int n = 0; n < this.size(); n++)
        {
            Person person = (Person) this.get(n);
            if(person.compareTo(search) == 0)
                persons.add(person);
        }

        return persons;

    }

}