package com.example.jasmine.aoa;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder>{

    private List<Person> addressList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, address;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            address = (TextView) view.findViewById(R.id.address);
        }
    }


    public AddressAdapter(List<Person> addressList) {
        this.addressList = addressList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Person person = addressList.get(position);
        holder.name.setText(String.format("%s %s", person.getFirstname(), person.getLastname()));
        holder.address.setText(person.getAddress());
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }

}
