package com.example.jasmine.aoa;

public class Person implements Comparable<Person>{

    private String Firstname;
    private String Lastname;
    private String Address;

    private Person left;
    private Person right;

    public Person(String firstname, String lastname, String address) {
        Firstname = firstname;
        Lastname = lastname;
        Address = address;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String firstname) {
        Firstname = firstname;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public Person getLeft() {
        return left;
    }

    public void setLeft(Person left) {
        this.left = left;
    }

    public Person getRight() {
        return right;
    }

    public void setRight(Person right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return "Person{" +
                "Firstname='" + Firstname + '\'' +
                ", Lastname='" + Lastname + '\'' +
                ", Address='" + Address + '\'' +
                '}';
    }

    @Override
    public int compareTo(Person person) {

        int i = Lastname.compareToIgnoreCase(person.getLastname());
        if (i != 0)
            return i;

        i = Firstname.compareToIgnoreCase(person.getFirstname());
        if (i != 0)
            return i;

        return 0;
    }
}
